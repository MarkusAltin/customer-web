import React, { useState, useEffect }from 'react';
import Carview from './components/Carview';
import Orderview from './components/Orderview';
import Editorderview from './components/Editorderview';
import './index.css'
import Loginview from './components/Loginview';
import moment from 'moment';

function App() {
  const [cars, setCars] = useState([])
  const [orders, setOrders] = useState([])
  const [orderToEdit, setOrderToEdit] = useState();
  const [loggedIn, setLoggedIn] = useState();
  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [carToShow, setCarToShow] = useState();
  const [orderDetails, setOrderDetails] = useState(false);
  let headers = new Headers();
  headers.set('Authorization','Basic ' + btoa(username + ':' + password));

  useEffect(() => {
    if (loggedIn) {
    const getCars = async () => {
      const carsFromServer = await fetchCars()
      setCars(carsFromServer);
    }
    getCars();
    const getOrders = async () => {
      const ordersFromServer = await fetchOrders()
      setOrders(ordersFromServer);
    }
    getOrders();
  }
  }, [loggedIn])

  const fetchCars = async () => {
    headers.append('Content-Type', 'text/json');
    const res = await fetch('http://localhost:8081/api/v1/cars', {
      method:'GET',
      headers:headers
    })
    const data = await res.json();
    return data;
  }

  const fetchOrders = async () => {
    headers.append('Content-Type', 'text/json');
    const res = await fetch('http://localhost:8081/api/v1/myorders', {
      method:'GET',
      headers:headers
    })
    const data = await res.json();
    return data;
  }

  const rentCar = async (car, date) => {
    let orderDate = new Date(date);
    let currentDate = new Date();
    let bookedCar = false;
    orders.forEach(order => {
      let compareDate = moment(order.date).format('YYYY-MM-DD');
      let chosenDate = moment(date).format('YYYY-MM-DD');
      if (order.car.id === car.id && compareDate === chosenDate) {
        bookedCar = true;
      }
    });
    if (orderDate.getTime() < currentDate.getTime() || bookedCar) {
      alert("Bilen ej tillgänglig det datumet");
    } else {
      let order = {
        customer: {
          id: 4,
          name: username
        },
        car: car,
        date: date,
        active: true
      }
      headers.append('Content-Type', 'application/json');
      const res = await fetch('http://localhost:8081/api/v1/ordercar', {
        method:'POST',
        headers:headers,
        body: JSON.stringify(order)
      })
      const data = await res.json()
      if (res.status === 200) {
        alert("Bil bokad");
      }
    }
  }

  const editOrder = (order) => {
    if (order.active === true) {
      setOrderToEdit(order);
    }
  }

  const carFunc = (order) =>  {
    cars.forEach(car => {
      if (order.car != null) {
        if (car.id === order.car.id) {
          setCarToShow(car);
        }
      }
  });
    if (carToShow) {
      setOrderDetails(true);
    }
  }

  const saveOrder = async (order) => {
    headers.append('Content-Type', 'application/json');
    const res = await fetch('http://localhost:8081/api/v1/updateorder', {
      method:'PUT',
      headers:headers,
      body: JSON.stringify(order)
    })
    const data = await res.json()
  }

  const logIn = (username, password) => {
    setUsername(username);
    setPassword(password);
    if (username === "user" && password === "password") {
      setLoggedIn(true);
    } else {
      alert("Fel användarnamn eller lösenord");
    }
  }
  const logOut = () => {
    setLoggedIn(false);
  }

  return (
    <div className="container">
    {loggedIn === false || loggedIn === undefined ? 
    <Loginview logIn={logIn} username={username} password={password}/> :
    <>
      <div className="links">
            <ul>
                <div className="logout-button">
                <button type="button" className="standard-button" onClick={logOut}>Logga ut</button>
            </div>
            </ul>
        </div>
      <Carview cars={cars} rentCar={rentCar}/> <br/>
      <Orderview orders={orders} editOrder={editOrder} cars={cars} 
      orderDetails={orderDetails} carToShow={carToShow} carFunc={carFunc}/> <br/>
      <Editorderview orderToEdit={orderToEdit} cars={cars} saveOrder={saveOrder}/> </> }
      </div>
  )
}

export default App;
