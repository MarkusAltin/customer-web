import React from 'react'
import moment from 'moment'

export default function Orderview({orders, editOrder, orderDetails, carToShow, carFunc}) {

    function checkNull (order) {
        if(order.car == null) {
            return 'none';
        } else {
            return order.car.id;
        }
    }

    return (
        <form>
            <div className="orders">
                <h4 id="columns">Order ID Kund ID Bil ID&ensp;Datum</h4>
                    {orders.map((order) => (
                        <div key={order.id} className="textfields">
                            <h4 id="table">&ensp;{order.id}&emsp;&emsp;&emsp;&emsp;
                            {order.customer.id}&emsp;&emsp;&emsp;&emsp;{checkNull(order)}
                            &emsp;{moment(order.date).format('YYYY-MM-DD')}</h4>
                            { orderDetails === true && order.car.id === carToShow.id ?
                                <h4>Märke: {carToShow.brand}, Storlek: {carToShow.size}, Pris: {carToShow.price}</h4>
                                : '' 
                            }
                            <div className="button">
                                <button type="button" className="submit" onClick={() => carFunc(order)}>Detaljer</button>
                                <button type="button" className="submit" onClick={() => editOrder(order)}>Redigera</button>
                            </div>
                        </div>
                    ))}
            </div>
        </form>
    )
}
