import React, {useState} from 'react'
import moment from 'moment'

export default function Editorderview({ orderToEdit, cars, saveOrder }) {
    const [date, setDate] = useState();

    function dateChange (event) {
        orderToEdit.date = event.target.value;
        setDate(event.target.value);
    }
    function carChange (event) {
        const selectCar = event.target.value;
        const chosenCar = selectCar.split(",");
        cars.forEach(car => {
            if (car.brand === chosenCar[0] && car.size === chosenCar[1]) {
                orderToEdit.car = car;
            }
        });
        console.log(orderToEdit.car);
    }
    
    function checkNull (order) {
        if(order.car === null) {
            return 'null';
        } else {
            const orderCar = order.car.brand + ',' + order.car.size;
            return orderCar;
        }
    }

     return (
       <form>
           {orderToEdit === undefined ? '' :
        <div className="form-div">
           <div className="textfields">
                <label id="orderid">Order ID: {orderToEdit.id}</label><br/><br/>
                <label id="customerid">Kund ID: {orderToEdit.customer.id}</label><br/><br/>
                <label id="carID">Car: </label>
                <select label={checkNull(orderToEdit)} id="carSelect" onChange={carChange}>
                    <option key={orderToEdit.car.id}>{checkNull(orderToEdit)}</option>
                    {cars.map((car) => (
                        <option key={car.id}>{car.brand},{car.size}</option>
                    ))}
                </select>
                <br/><br/>
                <input type="date" id="Test_DateLocal" value={moment(orderToEdit.date).format('YYYY-MM-DD')} onChange={dateChange}/>
            </div><br/>
        <div className="button">
            <button type="submit" className="submit" onClick={() => saveOrder(orderToEdit)}>Spara</button>
        </div>
       </div> }
    </form>
    )
}
