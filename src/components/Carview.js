import React from 'react'

export default function carview({cars, rentCar}) {

    let date;

    function dateChange (event) {
        console.log(event.target.value);
        date = event.target.value;
    }

    return (
        <form>
        <div className="cars">
        <h4 id="columns">Märke&ensp;Storlek&ensp;Pris</h4>
            {cars.map((car) => (
                <div key={car.id} className="textfields">
                    <h4 id="table" >{car.brand}&emsp;{car.size}&emsp;{car.price}</h4>
                    <div className="button">
                        <input type="date" id="Date" onChange={dateChange}/>
                        <button type="button" className="submit" onClick={() => rentCar(car, date)}>Hyr</button>
                    </div>
                </div>
            ))}
        </div>
        </form>
    )
}
